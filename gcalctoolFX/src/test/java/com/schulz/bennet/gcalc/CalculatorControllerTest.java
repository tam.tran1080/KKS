package com.schulz.bennet.gcalc;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import org.junit.Test;
import static org.loadui.testfx.Assertions.verifyThat;
import org.loadui.testfx.GuiTest;
import static org.loadui.testfx.controls.Commons.hasText;

/**
 *
 * @author bennet-schulz.com
 */
public class CalculatorControllerTest extends GuiTest {

    @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = FXMLLoader.load(getClass().getResource("gcalctoolFX.fxml"));
        } catch (IOException ex) {
            Logger.getLogger(CalculatorControllerTest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return parent;
    }

    @Test
    public void testAddition() {
        Button one = find("#one");
        Button plus = find("#plus");
        Button two = find("#two");

        Button equalSign = find("#equal");

        click(one);
        click(plus);
        click(two);

        verifyThat("#display", hasText("1+2"));
        click(equalSign);
        verifyThat("#display", hasText("3.0"));
    }

    @Test
    public void testSubtraction() {
        Button one = find("#one");
        Button minus = find("#minus");
        Button two = find("#two");

        Button equalSign = find("#equal");

        click(two);
        click(minus);
        click(one);

        verifyThat("#display", hasText("2-1"));
        click(equalSign);
        verifyThat("#display", hasText("1.0"));
    }

    @Test
    public void testMultiplication() {
        Button two = find("#two");
        Button times = find("#times");
        Button three = find("#three");

        Button equalSign = find("#equal");

        click(two);
        click(times);
        click(three);

        verifyThat("#display", hasText("2x3"));
        click(equalSign);
        verifyThat("#display", hasText("6.0"));
    }

    @Test
    public void testDivision() {
        Button six = find("#six");
        Button divide = find("#divide");
        Button three = find("#two");

        Button equalSign = find("#equal");

        click(six);
        click(divide);
        click(three);

        verifyThat("#display", hasText("6÷2"));
        click(equalSign);
        verifyThat("#display", hasText("3.0"));
    }

    @Test
    public void testSqrt() {
        Button six = find("#six");
        Button sqrt = find("#sqrt");

        Button equalSign = find("#equal");

        click(sqrt);
        click(six);

        verifyThat("#display", hasText("√6"));
        click(equalSign);
        verifyThat("#display", hasText("2.449489742783178"));
    }

    @Test
    public void testSquare() {
        Button four = find("#four");
        Button two = find("#two");
        Button square = find("#square");

        Button equalSign = find("#equal");

        click(four);
        click(two);
        click(square);

        verifyThat("#display", hasText("42²"));
        click(equalSign);
        verifyThat("#display", hasText("1764.0"));
    }

    @Test
    public void testClearButton() {
        Button four = find("#four");
        Button two = find("#two");
        Button square = find("#square");

        Button clear = find("#clear");

        click(four);
        click(two);
        click(square);

        verifyThat("#display", hasText("42²"));
        click(clear);
        verifyThat("#display", hasText(""));
    }

    @Test
    public void testParenthesis() {
        Button leftparenthesis = find("#leftparenthesis");
        Button two = find("#two");
        Button plus = find("#plus");
        Button three = find("#three");
        Button rightparenthesis = find("#rightparenthesis");

        Button clear = find("#clear");

        click(leftparenthesis);
        click(two);
        click(plus);
        click(three);
        click(rightparenthesis);

        verifyThat("#display", hasText("(2+3)"));
    }
}
