package com.schulz.bennet.gcalc;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

/**
 *
 * @author bennet-schulz.com
 */
public class CalculatorController {

    @FXML
    private TextField display;

    @FXML
    private Button square;

    @FXML
    private Button sqrt;

    @FXML
    private Button leftparenthesis;

    @FXML
    private Button rightparenthesis;

    @FXML
    private void handleButtonAction(ActionEvent e) {
        Button source = (Button) e.getSource();
        String displayText = display.getText();
        if (source == leftparenthesis) {
            display.setText("(" + displayText);
        } else if (source == rightparenthesis) {
            display.setText(displayText + ")");
        }
        if (source == square) {
            display.setText(displayText + "²");
        } else if (source == sqrt) {
            display.setText(displayText.isEmpty() ? displayText + "√" : "√" + displayText);
        } else {
            display.setText(displayText + source.getText());
        }
    }

    @FXML
    private void handleRemoveButtonAction(ActionEvent e) {
        display.setText("");
    }

    @FXML
    private void handleCalculationAction(ActionEvent e) {
        String displayText = display.getText();
        int textLength = displayText.length();
        String result = "";

        if (displayText.contains("+")) {
            int plusIndex = displayText.indexOf("+");
            Double a = Double.valueOf(displayText.substring(0, plusIndex));
            Double b = Double.valueOf(displayText.substring(plusIndex + 1, textLength));
            result = String.valueOf((a + b));
        } else if (displayText.contains("-")) {
            int minusIndex = displayText.indexOf("-");
            Double a = Double.valueOf(displayText.substring(0, minusIndex));
            Double b = Double.valueOf(displayText.substring(minusIndex + 1, textLength));
            result = String.valueOf((a - b));
        } else if (displayText.contains("x")) {
            int multiplyIndex = displayText.indexOf("x");
            Double a = Double.valueOf(displayText.substring(0, multiplyIndex));
            Double b = Double.valueOf(displayText.substring(multiplyIndex + 1, textLength));
            result = String.valueOf((a * b));
        } else if (displayText.contains("÷")) {
            int divideIndex = displayText.indexOf("÷");
            Double a = Double.valueOf(displayText.substring(0, divideIndex));
            Double b = Double.valueOf(displayText.substring(divideIndex + 1, textLength));
            result = String.valueOf((a / b));
        } else if (displayText.contains("²")) {
            int squareIndex = displayText.indexOf("²");
            Double v = Double.valueOf(displayText.substring(0, squareIndex));
            result = String.valueOf(Math.pow(v, 2));
        } else if (displayText.contains("√")) {
            int sqrtIndex = displayText.indexOf("√");
            Double a = Double.valueOf(displayText.substring(sqrtIndex + 1, displayText.length()));
            result = String.valueOf(Math.sqrt(a));
        }
        display.setText(result);
    }
}
