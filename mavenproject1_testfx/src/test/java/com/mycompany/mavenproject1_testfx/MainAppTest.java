
package com.mycompany.mavenproject1_testfx;

import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import javafx.util.Duration;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Test;
import org.loadui.testfx.GuiTest;
import org.loadui.testfx.*;
import static org.loadui.testfx.Assertions.assertNodeExists;
import static org.loadui.testfx.Assertions.verifyThat;
import static org.loadui.testfx.controls.Commons.hasText;
import org.loadui.testfx.utils.Matchers.*;

public class MainAppTest extends GuiTest {
    
    public MainAppTest() {
    }
   
  @Override
    protected Parent getRootNode() {
        Parent parent = null;
        try {
            parent = new MainApp.CounterPane();
            return parent;
        } catch (Exception ex) {
            System.out.println("poikkeus");
        }
        return parent;
    }
    /**
     * Test of start method, of class MainApp.
     * @throws java.lang.Exception
     */
    
    public void testStart() throws Exception {
        System.out.println("start");
        Stage stage = null;
        MainApp instance = new MainApp();
        instance.start(stage);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of main method, of class MainApp.
     */
    @Test
    public void verifying_data() {        

        verifyThat("#email", hasText("email"));
        verifyThat("#passwd", hasText("passwd"));
        verifyThat("#logstatus", hasText("Required information is missing"));
    }

    @Test
    public void login_submit(){
        
        click("login");
        verifyThat("#logstatus", hasText("logging in..."));
    }

}
