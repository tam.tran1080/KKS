package com.mycompany.mavenproject1_testfx;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXML;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.stage.Stage;
import javafx.scene.layout.StackPane;
import javafx.geometry.Insets;



public class MainApp extends Application {
    @FXML
    private Button count, login; 
     
    @FXML
    private TextField countValue, email, passwd, logstatus;
     
   public static class CounterPane extends StackPane {  
        public CounterPane() {
            super();
            
            // create email and passwd field and button for them
            TextField email = new TextField("email");
            email.setId("email");
            TextField passwd = new TextField("passwd");
            passwd.setId("passwd");
            
            Button login = new Button("login");
            login.setId("login");
            
            // Shows the status of the application
            TextField logstatus = new TextField("Required information is missing");
            logstatus.setId("logstatus");
            
            // When the button is pushed, the status will change
            login.setOnAction(event ->{

                logstatus.setText("Logging in...");
                
            });
            
            // Fields in the application
            email.setEditable(true);
            email.setPrefWidth(150);
            passwd.setEditable(true);
            passwd.setPrefWidth(150);
            logstatus.setEditable(false);
            logstatus.setPrefWidth(200);
            
            
            // create and add containers.
            HBox box = new HBox(10, email, passwd, login, logstatus); //countButton, countValue
            box.setPadding(new Insets(10));
            box.setAlignment(Pos.CENTER);
            getChildren().add(box);
        }
    }

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = new CounterPane();

        Scene scene = new Scene(root,200,50);
        scene.getStylesheets().add("/styles/Styles.css");
        
        stage.setTitle("JavaFX and Maven");
        stage.setScene(scene);
        stage.show();
    }

    public static void main(String[] args) {
        launch(args);
    }

}
